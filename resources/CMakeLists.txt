add_custom_target(resources ALL COMMAND ${CMAKE_COMMAND} -E echo "")
add_subdirectory(graphics)
add_subdirectory(sound)
add_subdirectory(data)
