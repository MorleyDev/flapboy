include(files.cmake)

add_empty_target(graphics)
add_dependencies(resources graphics)

function(add_svg_images)
	foreach(NAME ${ARGN})
		add_custom_command(TARGET graphics COMMAND inkscape -f ${CMAKE_CURRENT_LIST_DIR}/${NAME}.svg -e ${NAME}.png)
		install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${NAME}.png DESTINATION resources/graphics)
	endforeach(NAME)
endfunction(add_svg_images)

function(add_png_images)
	foreach(NAME ${ARGN})
		install(FILES ${CMAKE_CURRENT_LIST_DIR}/${NAME}.png DESTINATION resources/graphics)
	endforeach(NAME)
endfunction(add_png_images)

function(add_vert_shaders)
	foreach(NAME ${ARGN})
		install(FILES ${CMAKE_CURRENT_LIST_DIR}/${NAME}.vert DESTINATION resources/graphics)
	endforeach(NAME)
endfunction(add_vert_shaders)

function(add_frag_shaders)
	foreach(NAME ${ARGN})
		install(FILES ${CMAKE_CURRENT_LIST_DIR}/${NAME}.frag DESTINATION resources/graphics)
	endforeach(NAME)
endfunction(add_frag_shaders)

add_svg_images(${RESOURCES_GRAPHICS_SVG_FILES})
add_png_images(${RESOURCES_GRAPHICS_PNG_FILES})
add_vert_shaders(${RESOURCES_GRAPHICS_VERT_SHADERS})
add_frag_shaders(${RESOURCES_GRAPHICS_FRAG_SHADERS})
