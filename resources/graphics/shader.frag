uniform sampler2D texture;
uniform vec4 wColour;
uniform vec4 rColour;
uniform vec4 gColour;
uniform vec4 bColour;

void main()
{
	vec4 rgb = texture2D(texture, gl_TexCoord[0].xy);
	if (rgb.a == 0)
		gl_FragColor = rgb;
	else if (rgb == vec4(1.0, 0.0, 1.0, 1.0))
		gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
	else if ( rgb == vec4(1.0, 1.0, 1.0, 1.0) )
		gl_FragColor = wColour;
	else if (rgb.r == 1.0)
		gl_FragColor = rColour;
	else if (rgb.g == 1.0)
		gl_FragColor = gColour;
	else if (rgb.b == 1.0)
		gl_FragColor = bColour;
	else
		gl_FragColor = rgb;
}
