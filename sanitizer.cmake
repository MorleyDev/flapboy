####################################################
# Allow the enabling of various sanitizers support #
####################################################

option(ENABLE_THREAD_SANITIZER "Enable thread-sanitizer support" OFF)
option(ENABLE_ADDRESS_SANITIZER "Enable address sanitizer support" OFF)

if(ENABLE_THREAD_SANITIZER)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=thread -shared -fPIC")
	set(CMAKE_EXE_LINKER_FLAGS="${CMAKE_EXE_LINKER_FLAGS} -fsanitize=thread")
endif()

if (ENABLE_ADDRESS_SANITIZER)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address")
	set(CMAKE_EXE_LINKER_FLAGS="${CMAKE_EXE_LINKER_FLAGS} -fsanitize=address")
endif()

