
macro(target_link_debug_libraries TRGT)
	foreach(DEP ${ARGN})
		target_link_libraries(${TRGT} debug ${DEP})
	endforeach()
endmacro(target_link_debug_libraries)

function(target_link_optimized_libraries TRGT)
	foreach(DEP ${ARGN})
		target_link_libraries(${TRGT} optimized ${DEP})
	endforeach()
endfunction(target_link_optimized_libraries)

function(add_empty_target TRGT)
    add_custom_target(${TRGT} DEPENDS ${TRGT}.tmp COMMAND ${CMAKE_COMMAND} -E touch ${ZND_DEP_ARG}_zander.tmp)
    add_custom_command(OUTPUT ${TRGT}.tmp COMMAND ${CMAKE_COMMAND} -E echo "")
endfunction(add_empty_target)
