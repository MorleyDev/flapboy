#ifndef APP_VIEW_BACKGROUNDVIEW_HPP_INCLUDEd
#define APP_VIEW_BACKGROUNDVIEW_HPP_INCLUDEd

#include <ppr/view/View.hpp>
#include <ppr/model/Texture.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <random>
#include <list>
#include <chrono>
#include <atomic>

namespace app {
	namespace view {
		class BackgroundView : public ppr::view::View {
		private:
			std::unique_ptr<ppr::model::Texture> m_background;
			std::atomic<bool> m_isInGame;

		public:
			explicit BackgroundView(ppr::data::ContentLoader& loader);

			virtual void draw(ppr::data::Renderer&);
		};
	}
}

#endif//APP_VIEW_BACKGROUNDVIEW_HPP_INCLUDEd
