#ifndef APP_VIEW_BIRDVIEW_HPP_INCLUDED
#define APP_VIEW_BIRDVIEW_HPP_INCLUDED

#include <ppr/view/View.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <ppr/data/SoundEffectPlayer.hpp>
#include <ppr/model/Texture.hpp>
#include <ppr/model/rectangle.hpp>
#include <ppr/model/SoundEffect.hpp>

#include "../model/Bird.hpp"
#include "../tl/Sync.hpp"

#include <memory>

namespace app {
	namespace view {
	    class BirdView : public ppr::view::View {
	    private:
		    ppr::data::SoundEffectPlayer& m_sound;
		    std::unique_ptr<ppr::model::Texture> m_spritesheet;
		    std::unique_ptr<ppr::model::SoundEffect> m_cheep;
		    std::unique_ptr<ppr::model::SoundEffect> m_hitPipe;
		    app::tl::Sync<app::model::Bird> const& m_bird;
		    ppr::model::recti m_birdSpriteArea;

	    public:
		    BirdView(ppr::data::SoundEffectPlayer& sound, ppr::data::ContentLoader& content, app::tl::Sync<app::model::Bird> const& bird);

		    virtual void draw(ppr::data::Renderer& r);
	    };
	}
}

#endif//APP_VIEW_BIRDVIEW_HPP_INCLUDED
