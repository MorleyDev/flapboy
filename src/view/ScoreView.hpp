#ifndef APP_VIEW_SCOREVIEW_HPP_INCLUDED
#define APP_VIEW_SCOREVIEW_HPP_INCLUDED

#include <ppr/data/FileSystem.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <ppr/view/View.hpp>
#include <ppr/model/SoundEffect.hpp>
#include <ppr/model/Texture.hpp>

#include <atomic>
#include <memory>

namespace app {
    namespace view {
        class ScoreView : public ppr::view::View {
        private:
	        std::atomic<bool> m_drawScore;
	        std::atomic<std::size_t> const& m_score;
	        ppr::data::SoundEffectPlayer& m_sound;
	        std::unique_ptr<ppr::model::SoundEffect> m_scoreSound;
	        std::unique_ptr<ppr::model::Texture> m_spritesheet;

        public:
	        ScoreView(std::atomic<std::size_t> const& score, ppr::data::SoundEffectPlayer& sound, ppr::data::ContentLoader& content);

	        virtual void draw(ppr::data::Renderer&);
        };
    }
}

#endif//APP_VIEW_SCOREVIEW_HPP_INCLUDED
