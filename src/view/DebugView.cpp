#include "DebugView.hpp"
#include "../events/ToggleDebugViewEvent.hpp"

#include <ppr/data/Renderer.hpp>
#include <limits>

app::view::DebugView::DebugView(tl::Sync<model::Bird> const& bird, tl::Sync<std::list<model::Pipe>> const& pipes)
		: ppr::view::View(std::numeric_limits<std::int32_t>::max()),
		  m_isEnabled(false),
		  m_bird(bird),
		  m_pipes(pipes) {

	onEvent<events::ToggleDebugViewEvent>([this](events::ToggleDebugViewEvent) {
		bool curr;
	    do { curr = m_isEnabled.load(); } while(m_isEnabled.exchange(!curr) != curr);
	});

}

void app::view::DebugView::draw(ppr::data::Renderer& r) {
	if (!m_isEnabled.load())
		return;

	r.drawRectangle(ppr::model::rectf(0.0,0.0,160.0,144.0), ppr::model::colour(0,0,255), false);

	drawBirdCollisionBox(r);
	drawPipesCollisionBoxes(r);
}

void app::view::DebugView::drawBirdCollisionBox(ppr::data::Renderer& r) {
	auto const bird = m_bird.get();
	const ppr::model::rectf birdArea(bird.position.x - 9.0, bird.position.y - 5.0, 18.0, 10.0);
	r.drawRectangle(birdArea, ppr::model::colour(0,0,255), false);
}

void app::view::DebugView::drawPipesCollisionBoxes(ppr::data::Renderer& r) {
	auto const pipes = m_pipes.get();
	for(const auto pipe : pipes ){
		const ppr::model::rectf topMiddlePipeArea(pipe.x - 10.0, 0.0, 20.0, pipe.topHeight);
		const ppr::model::rectf topHeadPipeArea(pipe.x - 10.0, pipe.topHeight, 20.0, 7.0);
		const ppr::model::rectf bottomMiddlePipeArea(pipe.x - 10.0, 144.0 - pipe.bottomHeight, 20.0, pipe.bottomHeight);
		const ppr::model::rectf bottomHeadPipeArea(pipe.x - 10.0, 144.0 - pipe.bottomHeight - 7.0, 20.0, 7.0);
		
		r.drawRectangle(topMiddlePipeArea, ppr::model::colour(255,0,255), false);
		r.drawRectangle(topHeadPipeArea, ppr::model::colour(255,0,0), false);
		
		r.drawRectangle(bottomMiddlePipeArea, ppr::model::colour(255,0,255), false);
		r.drawRectangle(bottomHeadPipeArea, ppr::model::colour(255,0,0), false);
	}
}
