#include "BirdView.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/BirdJumpedEvent.hpp"
#include "../events/GameOverEvent.hpp"
#include <ppr/data/FileSystem.hpp>
#include <ppr/data/loaders/TextureLoader.hpp>
#include <ppr/data/loaders/SoundEffectLoader.hpp>

app::view::BirdView::BirdView(ppr::data::SoundEffectPlayer& sound,
                              ppr::data::ContentLoader& content,
                              tl::Sync<app::model::Bird> const& bird)
		: m_sound(sound),
		  m_spritesheet(content.load<ppr::model::Texture>("graphics/spritesheet.png")),
		  m_cheep(content.load<ppr::model::SoundEffect>("sound/cheep.wav")),
		  m_hitPipe(content.load<ppr::model::SoundEffect>("sound/hit.wav")),
		  m_bird(bird),
		  m_birdSpriteArea(0,0,20,12) {
	onEvent<events::StartGameEvent>([this](events::StartGameEvent e) {
	    switch(e.difficulty) {
		    case model::Difficulty::Easy:
			    m_birdSpriteArea.x = 0;
	            m_birdSpriteArea.y = 0;
	            break;
		    case model::Difficulty::Medium:
			    m_birdSpriteArea.x = 0;
	            m_birdSpriteArea.y = 20;
	            break;
		    case model::Difficulty::Hard:
			    m_birdSpriteArea.x = 40;
	            m_birdSpriteArea.y = 20;
	            break;
	    }
	});
	onEvent<events::BirdJumpedEvent>([this](events::BirdJumpedEvent) {
	    m_sound.play(*m_cheep, ppr::model::pos2f(0.0, 0.0), 0.2);
	});
	onEvent<events::GameOverEvent>([this](events::GameOverEvent) {
	    m_sound.play(*m_hitPipe, ppr::model::pos2f(0.0, 0.0));
	});
}

void app::view::BirdView::draw(ppr::data::Renderer& r) {
	const auto bird = m_bird.get();
	ppr::model::recti animationFrameSpriteArea = m_birdSpriteArea; animationFrameSpriteArea.x += 20;
	const ppr::model::rectf destArea(bird.position.x - 10, bird.position.y - 6, 20, 12);

	r.drawTexture(*m_spritesheet, destArea, bird.animation > 0.0 ? animationFrameSpriteArea : m_birdSpriteArea);
}
