#include "ScoreView.hpp"
#include "../events/AwardPointEvent.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/EndGameEvent.hpp"

#include <ppr/data/SpriteBatch.hpp>
#include <ppr/data/loaders/TextureLoader.hpp>
#include <ppr/data/loaders/SoundEffectLoader.hpp>
#include <ppr/data/FileSystem.hpp>
#include <ppr/tl/to_string.hpp>

app::view::ScoreView::ScoreView(std::atomic<std::size_t> const& score, ppr::data::SoundEffectPlayer& sound, ppr::data::ContentLoader& content)
	: m_drawScore(false),
	  m_score(score),
	  m_sound(sound),
	  m_scoreSound(content.load<ppr::model::SoundEffect>("sound/point.wav")),
      m_spritesheet(content.load<ppr::model::Texture>("graphics/spritesheet.png")) {
	onEvent<events::AwardPointEvent>([this](events::AwardPointEvent) {
	    m_sound.play(*m_scoreSound, ppr::model::pos2f(0.0,0.0), 0.2);
	});
	onEvent<events::StartGameEvent>([this](events::StartGameEvent) {
		m_drawScore.store(true);
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent) {
	    m_drawScore.store(false);
	});
}

void app::view::ScoreView::draw(ppr::data::Renderer& r) {
	if (!m_drawScore.load())
		return;

	const ppr::model::recti source(40, 0, 8, 12);

	ppr::model::rectf destination(0.0, 0.0, 8.0, 12.0);

	auto scoreString = ppr::tl::to_string(m_score.load());

	auto batch = r.createBatch(*m_spritesheet);
	for(auto i = 0; i < scoreString.length(); ++i) {
		auto val = static_cast<int>(scoreString[i] - '0');
		ppr::model::recti spriteSource = source; spriteSource.x += val * 8;

		batch->draw(destination, spriteSource);
		destination.x += 8.0;
	}
}
