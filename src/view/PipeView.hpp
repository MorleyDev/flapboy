#ifndef APP_VIEW_PIPEVIEW_HPP_INCLUDED
#define APP_VIEW_PIPEVIEW_HPP_INCLUDED

#include <ppr/view/View.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <ppr/model/Texture.hpp>

#include "../model/Pipe.hpp"
#include "../tl/Sync.hpp"

#include <list>
#include <memory>

namespace app {
	namespace view {
	    class PipeView : public ppr::view::View {
	    private:
		    std::unique_ptr<ppr::model::Texture> m_spritesheet;
		    app::tl::Sync<std::list<app::model::Pipe>> const& m_pipes;

	    public:
		    PipeView(ppr::data::ContentLoader& content, app::tl::Sync<std::list<app::model::Pipe>> const& pipes);

		    virtual void draw(ppr::data::Renderer& r);
	    };
	}
}

#endif//APP_VIEW_PIPEVIEW_HPP_INCLUDED
