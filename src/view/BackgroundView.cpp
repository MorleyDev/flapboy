#include "BackgroundView.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/EndGameEvent.hpp"
#include <ppr/model/Texture.hpp>
#include <ppr/data/Renderer.hpp>
#include <ppr/data/FileSystem.hpp>
#include <ppr/data/loaders/TextureLoader.hpp>

app::view::BackgroundView::BackgroundView(ppr::data::ContentLoader& loader)
		: ppr::view::View(-1000),
		  m_background(loader.load<ppr::model::Texture>("graphics/bg.png")),
		  m_isInGame(false) {
	onEvent<events::StartGameEvent>([this](events::StartGameEvent) {
	    m_isInGame.store(true);
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent) {
	    m_isInGame.store(true);
	});
}

void app::view::BackgroundView::draw(ppr::data::Renderer& r) {

	if (!m_isInGame.load())
		return;

	r.drawTexture(*m_background, 
	              ppr::model::rectf(0.0, 0.0, 160.0, 144.0), 
				  ppr::model::recti(0, 0, 160, 144));
}
