#include "TitleScreenView.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/EndGameEvent.hpp"
#include "../events/ChangeMenuOptionEvent.hpp"

#include <ppr/data/SpriteBatch.hpp>
#include <ppr/data/FileSystem.hpp>
#include <ppr/data/loaders/TextureLoader.hpp>
#include <ppr/data/loaders/SoundEffectLoader.hpp>
#include <ppr/tl/to_string.hpp>

app::view::TitleScreenView::TitleScreenView(tl::Sync<model::Highscore>& highscore,
							                ppr::data::SoundEffectPlayer& sound,
                                            ppr::data::ContentLoader& content)
		: m_highscore(highscore),
		  m_isTitleScreenEnabled(true),
		  m_activeMenuOption(0),
		  m_activeDifficulty(model::Difficulty::Easy),
		  m_sound(sound),
		  m_spritesheet(content.load<ppr::model::Texture>("graphics/spritesheet.png")),
		  m_titlescreen(content.load<ppr::model::Texture>("graphics/titlescreen.png")),
		  m_cheep(content.load<ppr::model::SoundEffect>("sound/cheep.wav")) {
	onEvent<events::StartGameEvent>([this](events::StartGameEvent) {
	    m_isTitleScreenEnabled.store(false);
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent) {
	    m_isTitleScreenEnabled.store(true);
	});
	onEvent<events::ChangeMenuOptionEvent>([this](events::ChangeMenuOptionEvent e) {
		m_activeMenuOption.store(e.option);
		m_activeDifficulty.store(e.difficulty);
		m_sound.play(*m_cheep, ppr::model::pos2f(0.0,0.0), 0.2);
	});
}

void app::view::TitleScreenView::draw(ppr::data::Renderer& r) {
	if (!m_isTitleScreenEnabled.load())
		return;

	r.drawTexture(*m_titlescreen, ppr::model::rectf(0.0,0.0,160.0,144.0), ppr::model::recti(0,0,160,144));

	auto batch = r.createBatch(*m_spritesheet);

	auto birdSprite = ppr::model::recti(0,0,20,12);
	switch(m_activeDifficulty.load()) {
		case model::Difficulty::Medium:
			birdSprite.y = 20;
			break;
		case model::Difficulty::Hard:
			birdSprite.x = 40;
			birdSprite.y = 20;
			break;
		default:
			break;
	}

	if (m_activeMenuOption == 0)
		batch->draw(ppr::model::rectf(70.0,102.0,20.0,12.0), birdSprite);
	else
		batch->draw(ppr::model::rectf(70.0,119.0,20.0,12.0), birdSprite);

	const ppr::model::recti source(40, 0, 8, 12);

	ppr::model::rectf destination(26.0, 130.0, 8.0, 12.0);

	model::Highscore highscore = m_highscore.get();
	std::string scoreString;
	switch (m_activeDifficulty.load()) {
		case model::Difficulty::Easy: scoreString = ppr::tl::to_string(highscore.easy); break;
		case model::Difficulty::Medium: scoreString = ppr::tl::to_string(highscore.medium); break;
		case model::Difficulty::Hard: scoreString = ppr::tl::to_string(highscore.hard); break;
	}

	for(auto i = 0; i < scoreString.length(); ++i) {
		auto val = static_cast<int>(scoreString[i] - '0');
		ppr::model::recti spriteSource = source; spriteSource.x += val * 8;

		batch->draw(destination, spriteSource);
		destination.x += 8.0;
	}
}
