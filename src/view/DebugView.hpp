#ifndef APP_VIEW_DEBUGVIEW_HPP_INCLUDED
#define APP_VIEW_DEBUGVIEW_HPP_INCLUDED

#include "../tl/Sync.hpp"
#include "../model/Bird.hpp"
#include "../model/Pipe.hpp"

#include <ppr/view/View.hpp>
#include <list>

namespace app {
	namespace view {
		class DebugView : public ppr::view::View {
		private:
			std::atomic<bool> m_isEnabled;
			tl::Sync<model::Bird> const& m_bird;
			tl::Sync<std::list<model::Pipe>> const& m_pipes;

		public:
			DebugView(tl::Sync<model::Bird> const& bird, tl::Sync<std::list<model::Pipe>> const& pipes);

			virtual void draw(ppr::data::Renderer& r);

		private:
			void drawBirdCollisionBox(ppr::data::Renderer& r);
			void drawPipesCollisionBoxes(ppr::data::Renderer& r);
		};
	}
}

#endif//
