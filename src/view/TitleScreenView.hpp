#ifndef APP_VIEW_TITLESCREENVIEW_HPP_INCLUDED
#define APP_VIEW_TITLESCREENVIEW_HPP_INCLUDED

#include "../model/Difficulty.hpp"
#include "../model/Highscore.hpp"
#include "../tl/Sync.hpp"

#include <ppr/view/View.hpp>
#include <ppr/model/Texture.hpp>
#include <ppr/model/SoundEffect.hpp>
#include <ppr/data/ContentLoader.hpp>

namespace app {
	namespace view {
		class TitleScreenView : public ppr::view::View {
		private:
			tl::Sync<model::Highscore>& m_highscore;
			std::atomic<bool> m_isTitleScreenEnabled;
			std::atomic<std::size_t> m_activeMenuOption;
			std::atomic<model::Difficulty> m_activeDifficulty;

			ppr::data::SoundEffectPlayer& m_sound;
			std::unique_ptr<ppr::model::Texture> m_spritesheet;
			std::unique_ptr<ppr::model::Texture> m_titlescreen;
			std::unique_ptr<ppr::model::SoundEffect> m_cheep;

		public:
			TitleScreenView(tl::Sync<model::Highscore>& highscore,
			                ppr::data::SoundEffectPlayer& sound,
			                ppr::data::ContentLoader& content);

			virtual void draw(ppr::data::Renderer& r);
		};
	}
}

#endif//APP_VIEW_TITLESCREENVIEW_HPP_INCLUDED
