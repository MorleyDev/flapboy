#include "PipeView.hpp"
#include <ppr/data/SpriteBatch.hpp>
#include <ppr/data/FileSystem.hpp>
#include <ppr/data/loaders/TextureLoader.hpp>
#include <ppr/data/loaders/SoundEffectLoader.hpp>

app::view::PipeView::PipeView(ppr::data::ContentLoader& content, app::tl::Sync<std::list<app::model::Pipe>> const& pipes)
		: m_spritesheet(content.load<ppr::model::Texture>("graphics/spritesheet.png")),
		  m_pipes(pipes) {
}

void app::view::PipeView::draw(ppr::data::Renderer& r) {
	const auto pipes = m_pipes.get();
	const ppr::model::recti topArea(0,12,20,7);
	const ppr::model::recti middleArea(0,19,20,1);

	auto batch = r.createBatch(*m_spritesheet);
	for(const auto pipe : pipes) {
		const ppr::model::rectf topMiddleDestArea(pipe.x - 10.0, 0.0, 20.0, pipe.topHeight);
		const ppr::model::rectf topHeadDestArea(pipe.x - 10.0, pipe.topHeight, 20.0, 7.0);

		const ppr::model::rectf bottomMiddleDestArea(pipe.x - 10.0, 144.0 - pipe.bottomHeight, 20.0, pipe.bottomHeight);
		const ppr::model::rectf bottomHeadDestArea(pipe.x - 10.0, 144.0 - pipe.bottomHeight - 7.0, 20.0, 7.0);

		batch->draw(topMiddleDestArea, middleArea);
		batch->draw(topHeadDestArea, topArea);
		batch->draw(bottomMiddleDestArea, middleArea);
		batch->draw(bottomHeadDestArea, topArea);
	}
}
