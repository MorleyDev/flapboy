#ifndef APP_TL_SYNC_HPP_INCLUDED
#define APP_TL_SYNC_HPP_INCLUDED

#include <mutex>

namespace app {
    namespace tl {
        template<typename T> class Sync {
        private:
	        T m_data;
	        mutable std::mutex m_mutex;
	        mutable std::mutex m_writeMutex;

        public:
	        Sync() : m_data(), m_mutex(), m_writeMutex() { }
	        Sync(T const& data) : m_data(data), m_mutex(), m_writeMutex() { }
	        Sync(T&& data) : m_data(std::move(data)), m_mutex(), m_writeMutex() { }

	        template<typename CALL> void update(CALL updateFunc) {
		        std::lock_guard<std::mutex> lockW(m_writeMutex); // Lock for writing
		        auto data = get();

		        data = updateFunc(data); // Update the data

		        std::lock_guard<std::mutex> lockR(m_mutex); // Lock for reading
		        m_data = data;
	        }

	        T get() const {
		        std::lock_guard<std::mutex> lockR(m_mutex);
		        return m_data;
	        }

	        void put(T data) {
		        std::lock_guard<std::mutex> lockW(m_writeMutex); // Lock for writing
		        std::lock_guard<std::mutex> lockR(m_mutex); // Lock for reading
		        m_data = data;
	        }
        };
    }
}

#endif// APP_TL_SYNC_HPP_INCLUDED
