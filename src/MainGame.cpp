#include "MainGame.hpp"

#include "view/DebugView.hpp"

#include "view/BackgroundView.hpp"
#include "view/BirdView.hpp"
#include "view/ScoreView.hpp"
#include "view/PipeView.hpp"
#include "view/TitleScreenView.hpp"

#include "ctrl/BirdController.hpp"
#include "ctrl/BirdPhysicsController.hpp"
#include "ctrl/BirdJumpController.hpp"
#include "ctrl/CollisionController.hpp"
#include "ctrl/ScoreController.hpp"
#include "ctrl/PipeController.hpp"
#include "ctrl/PipeScrollingController.hpp"
#include "ctrl/GameController.hpp"
#include "ctrl/TitleScreenController.hpp"
#include "ctrl/HighScoreController.hpp"

#include "events/RequestJumpBirdEvent.hpp"
#include "events/RequestSelectEvent.hpp"
#include "events/RequestMenuSwitchEvent.hpp"
#include "events/SpawnPipeEvent.hpp"
#include "events/StartGameEvent.hpp"
#include "events/ToggleDebugViewEvent.hpp"
#include "events/EndGameEvent.hpp"

#include "model/Controls.hpp"

#include <ppr/data/Window.hpp>
#include <ppr/data/InputSystem.hpp>
#include <ppr/data/Renderer.hpp>
#include <ppr/data/loaders/all.hpp>
#include <ppr/events/Close.hpp>

namespace {
    std::string readFileToString(ppr::data::ReadOnlyFile& file) {
	    auto size = file.size() - file.position();
	    std::unique_ptr<char[]> buffer(new char[size]);
	    size = file.read(buffer.get(), size);
	    return std::string(buffer.get(), size);
    }
	
	struct Palette {
		ppr::model::colour black;
		ppr::model::colour dark;
		ppr::model::colour mild;
		ppr::model::colour white;
	};
	
	template<typename ARCHIVE> void serialize(ARCHIVE& archive, Palette& p) {
		archive(ppr::tl::make_nvp("black", p.black));
		archive(ppr::tl::make_nvp("dark", p.dark));
		archive(ppr::tl::make_nvp("mild", p.mild));
		archive(ppr::tl::make_nvp("white", p.white));
	}
}

app::MainGame::~MainGame() {
}

app::MainGame::MainGame()
		: Game("FlapBoy #gbjam3"),
		  m_bird(),
		  m_pipes(),
		  m_score(0),
		  m_highscore() {

	auto controls = content.load<model::Controls>("data/controls.json");
	m_highscore.put(content.load<model::Highscore>("data/highscore.json"));

	window.onClose([this]() { events.enqueue(ppr::events::Close()); });

	renderer.setLogicalSize(ppr::model::rectf(0.0, 0.0, 160.0, 144.0));

	controllers.add(ppr::tl::make_unique<app::ctrl::BirdController>(m_bird));
	controllers.add(ppr::tl::make_unique<app::ctrl::BirdJumpController>(m_bird));
	controllers.add(ppr::tl::make_unique<app::ctrl::BirdPhysicsController>(m_bird));
	controllers.add(ppr::tl::make_unique<app::ctrl::CollisionController>(m_bird, m_pipes));
	controllers.add(ppr::tl::make_unique<app::ctrl::GameController>());
	controllers.add(ppr::tl::make_unique<app::ctrl::PipeController>(m_pipes));
	controllers.add(ppr::tl::make_unique<app::ctrl::PipeScrollingController>(m_pipes));
	controllers.add(ppr::tl::make_unique<app::ctrl::ScoreController>(m_score, m_bird, m_pipes));
	controllers.add(ppr::tl::make_unique<app::ctrl::HighScoreController>(m_highscore, m_score));
	controllers.add(ppr::tl::make_unique<app::ctrl::TitleScreenController>(content));

	auto palette = content.load<Palette>("data/palette.json");
	
	auto shader = std::shared_ptr<ppr::model::Shader>(content.load<ppr::model::Shader>("graphics/shader.frag", ppr::model::ShaderType::Fragment));
	shader->setCurrentTexture("texture");
	shader->set("wColour", palette.black);
	shader->set("rColour", palette.dark);
	shader->set("gColour", palette.mild);
	shader->set("bColour", palette.white);
	renderer.bindShader(*shader);
	
	views.add(ppr::tl::make_unique<app::view::BackgroundView>(content));
	views.add(ppr::tl::make_unique<app::view::BirdView>(sound, content, m_bird));
	views.add(ppr::tl::make_unique<app::view::PipeView>(content, m_pipes));
	views.add(ppr::tl::make_unique<app::view::ScoreView>(m_score, sound, content));
	views.add(ppr::tl::make_unique<app::view::TitleScreenView>(m_highscore, sound, content));
	views.add(ppr::tl::make_unique<app::view::DebugView>(m_bird, m_pipes));

	input.onMouseClick([this](std::int8_t button, ppr::data::InputSystem::ButtonState state, ppr::data::InputSystem::MousePosition) {
		if (state == ppr::data::InputSystem::ButtonState::Pressed)
			events.enqueue(app::events::RequestJumpBirdEvent());
	});
	input.onTouch([this](ppr::data::InputSystem::TouchPosition, ppr::data::InputSystem::ButtonState state) {
	    if (state == ppr::data::InputSystem::ButtonState::Pressed)
		    events.enqueue(app::events::RequestJumpBirdEvent());
	});
	input.onKeyboard([this, controls](std::int32_t key, ppr::data::InputSystem::ButtonState state) {
	    if (state == ppr::data::InputSystem::ButtonState::Pressed) {
		    if ( key == controls.jump )
		        events.enqueue(app::events::RequestJumpBirdEvent());
		    if ( key == controls.menuSwitch )
			    events.enqueue(app::events::RequestMenuSwitchEvent());
		    else if ( key == controls.select )
			    events.enqueue<events::RequestSelectEvent>({});
		    else if (key == controls.close )
			    events.enqueue<events::EndGameEvent>({});
		    else if ( key == controls.debug )
			    events.enqueue<events::ToggleDebugViewEvent>({});
	    }
	});
}
