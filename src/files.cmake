set(APP_FILES ${CMAKE_CURRENT_LIST_DIR}/main.cpp
              ${CMAKE_CURRENT_LIST_DIR}/MainGame.cpp
              ${CMAKE_CURRENT_LIST_DIR}/ctrl/BirdController.cpp
              ${CMAKE_CURRENT_LIST_DIR}/ctrl/BirdJumpController.cpp
              ${CMAKE_CURRENT_LIST_DIR}/ctrl/BirdPhysicsController.cpp
              ${CMAKE_CURRENT_LIST_DIR}/ctrl/CollisionController.cpp
              ${CMAKE_CURRENT_LIST_DIR}/ctrl/HighScoreController.cpp
              ${CMAKE_CURRENT_LIST_DIR}/ctrl/GameController.cpp
              ${CMAKE_CURRENT_LIST_DIR}/ctrl/PipeController.cpp
              ${CMAKE_CURRENT_LIST_DIR}/ctrl/PipeScrollingController.cpp
              ${CMAKE_CURRENT_LIST_DIR}/ctrl/ScoreController.cpp
              ${CMAKE_CURRENT_LIST_DIR}/ctrl/TitleScreenController.cpp
              ${CMAKE_CURRENT_LIST_DIR}/view/BackgroundView.cpp
              ${CMAKE_CURRENT_LIST_DIR}/view/BirdView.cpp
              ${CMAKE_CURRENT_LIST_DIR}/view/DebugView.cpp
              ${CMAKE_CURRENT_LIST_DIR}/view/ScoreView.cpp
              ${CMAKE_CURRENT_LIST_DIR}/view/PipeView.cpp
              ${CMAKE_CURRENT_LIST_DIR}/view/TitleScreenView.cpp)
