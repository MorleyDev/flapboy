#ifndef APP_MODEL_GAMESETTINGS_HPP_INCLUDED
#define APP_MODEL_GAMESETTINGS_HPP_INCLUDED

#include <ppr/tl/serialise.hpp>
#include <cstdint>

namespace app {
	namespace model {
		struct GameSettings {
			double pipeScrollSpeed = 50.0;
			double pipeGapSizeX = 50.0;
			std::size_t pipeCount = 4;

			std::size_t minGapSizeY = 50;
			std::size_t maxGapSizeY = 100;
			double pipeYWaveMultiplier = 50.0;
		};

		template<typename ARCHIVE> void serialize(ARCHIVE& archive, GameSettings& s) {
			archive(ppr::tl::make_nvp("scroll-speed", s.pipeScrollSpeed));
			archive(ppr::tl::make_nvp("pipe-gap-x", s.pipeGapSizeX));
			archive(ppr::tl::make_nvp("pipe-count", s.pipeCount));
			archive(ppr::tl::make_nvp("pipe-gap-y-min", s.minGapSizeY));
			archive(ppr::tl::make_nvp("pipe-gap-y-max", s.maxGapSizeY));
			archive(ppr::tl::make_nvp("pipe-y-wave-mult", s.pipeYWaveMultiplier));
		}
	}
}

#endif//APP_MODEL_GAMESETTINGS_HPP_INCLUDED
