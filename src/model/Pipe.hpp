#ifndef APP_MODEL_PIPE_HPP_INCLUDED
#define APP_MODEL_PIPE_HPP_INCLUDED

#include <ppr/model/position.hpp>

namespace app {
	namespace model {
		struct Pipe {
			double topHeight;
			double bottomHeight;
			double x;
			bool wasPassed;
		};
	}
}

#endif//APP_MODEL_PIPE_HPP_INCLUDED
