#ifndef APP_MODEL_DIFFICULTY_HPP_INCLUDED
#define APP_MODEL_DIFFICULTY_HPP_INCLUDED

namespace app {
	namespace model {
	    enum class Difficulty {
		    Easy,
		    Medium,
		    Hard
	    };
	}
}

#endif
