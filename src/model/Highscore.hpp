#ifndef APP_MODEL_HIGHSCORE_HPP_INCLUDED
#define APP_MODEL_HIGHSCORE_HPP_INCLUDED

#include <ppr/tl/serialise.hpp>
#include <cstdint>

namespace app {
	namespace model {
		struct Highscore {
			std::size_t easy = 0;
			std::size_t medium = 0;
			std::size_t hard = 0;
		};

		template<typename ARCHIVE> void serialize(ARCHIVE& a, Highscore& h) {
			a(ppr::tl::make_nvp("easy", h.easy));
			a(ppr::tl::make_nvp("medium", h.medium));
			a(ppr::tl::make_nvp("hard", h.hard));
		}
	}
}

#endif//APP_MODEL_HIGHSCORE_HPP_INCLUDED
