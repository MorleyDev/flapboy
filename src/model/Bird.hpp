#ifndef APP_MODEL_BIRD_HPP_INCLUDED
#define APP_MODEL_BIRD_HPP_INCLUDED

#include <ppr/model/position.hpp>

namespace app {
    namespace model {
        struct Bird {
	        ppr::model::pos2f position = ppr::model::pos2f(-100.0, 0.0);
	        double force = 0.0;
	        double animation = 0.0;
        };

    }
}

#endif//APP_MODEL_BIRD_HPP_INCLUDED
