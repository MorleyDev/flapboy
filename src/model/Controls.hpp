#ifndef APP_MODEL_CONTROLS_HPP_INCLUDED
#define APP_MODEL_CONTROLS_HPP_INCLUDED

namespace app {
	namespace model {
		struct Controls {
			std::size_t jump = 22;
			std::size_t debug = 15;
			std::size_t menuSwitch = 3;
			std::size_t close = 36;
			std::size_t select = 58;
		};
	    template<typename ARCHIVE> void serialize(ARCHIVE& archive, Controls& s) {
		    archive(ppr::tl::make_nvp("jump", s.jump));
		    archive(ppr::tl::make_nvp("debug", s.debug));
		    archive(ppr::tl::make_nvp("switch", s.menuSwitch));
		    archive(ppr::tl::make_nvp("close", s.close));
		    archive(ppr::tl::make_nvp("select", s.select));

	    }
	}
}

#endif//APP_MODEL_CONTROLS_HPP_INCLUDED
