#ifndef APP_MAINGAME_HPP_INCLUDED
#define APP_MAINGAME_HPP_INCLUDED

#include <ppr/Game.hpp>

#include "model/Bird.hpp"
#include "model/Pipe.hpp"
#include "model/Highscore.hpp"
#include "tl/Sync.hpp"

#include <list>

namespace app {
    class MainGame : public ppr::Game {
    private:
	    app::tl::Sync<app::model::Bird> m_bird;
	    app::tl::Sync<std::list<app::model::Pipe>> m_pipes;
	    std::atomic<std::size_t> m_score;
	    tl::Sync<model::Highscore> m_highscore;

    public:
	    virtual ~MainGame();

	    MainGame();
    };

}

#endif//APP_MAINGAME_HPP_INCLUDED
