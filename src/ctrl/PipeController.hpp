#ifndef APP_CTRL_PIPECONTROLLER_HPP_INCLUDED
#define APP_CTRL_PIPECONTROLLER_HPP_INCLUDED

#include "../tl/Sync.hpp"
#include "../model/Pipe.hpp"

#include <ppr/ctrl/Controller.hpp>

#include <list>
#include <random>
#include <mutex>

namespace app {
	namespace ctrl {
		class PipeController : public ppr::ctrl::Controller {
		private:
			double m_sineWave;
			std::atomic<double> m_sineWaveMultiplier;
			tl::Sync<std::list<model::Pipe>>& m_pipes;

			std::mutex m_rngMutex;
			std::default_random_engine m_randomEngine;
			std::uniform_int_distribution<std::size_t> m_pipeGapSizeGenerator;
			std::uniform_int_distribution<std::size_t> m_pipeHeightOffsetGenerator;

		public:
			PipeController(tl::Sync<std::list<model::Pipe>>& pipes);

			virtual void update(std::chrono::microseconds dt);

		private:
			model::Pipe genPipe();
		};
	}
}

#endif//APP_CTRL_PIPECONTROLLER_HPP_INCLUDED
