#include <fstream>
#include "HighScoreController.hpp"
#include "../events/EndGameEvent.hpp"
#include "../events/GameOverEvent.hpp"
#include "../events/StartGameEvent.hpp"

app::ctrl::HighScoreController::HighScoreController(app::tl::Sync<app::model::Highscore>& highscore, std::atomic<std::size_t>& score)
		: m_highscore(highscore),
		  m_score(score),
		  m_difficulty(model::Difficulty::Easy) {
	onEvent<events::StartGameEvent>([this](events::StartGameEvent event) {
	    std::lock_guard<std::mutex> lockFile(m_fileLock);
	    m_difficulty.store(event.difficulty);
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent)  { updateHighScore(); });
	onEvent<events::GameOverEvent>([this](events::GameOverEvent) { updateHighScore(); });
}

void app::ctrl::HighScoreController::update(std::chrono::microseconds dt) {

}

void app::ctrl::HighScoreController::updateHighScore() {
	m_highscore.update([this](app::model::Highscore hs) -> app::model::Highscore {
	    std::lock_guard<std::mutex> lockFile(m_fileLock);
	    std::size_t targetScore;
	    switch (m_difficulty.load()) {
		    case model::Difficulty::Easy:
			    targetScore = hs.easy;
	            break;
		    case model::Difficulty::Medium:
			    targetScore = hs.medium;
	            break;
		    case model::Difficulty::Hard:
			    targetScore = hs.hard;
	            break;
	    }
	    auto actualScore = m_score.load();
	    if (actualScore > targetScore) {
		    switch (m_difficulty.load()) {
			    case model::Difficulty::Easy:
				    hs.easy = actualScore;
		            break;
			    case model::Difficulty::Medium:
				    hs.medium = actualScore;
		            break;
			    case model::Difficulty::Hard:
				    hs.hard = actualScore;
		            break;
		    }

		    std::ofstream highscoreFile("resources/data/highscore.json");
		    if (highscoreFile.is_open())
			    highscoreFile << ppr::tl::to_json(hs) << std::endl;
	    }
	    return hs;
	});
}
