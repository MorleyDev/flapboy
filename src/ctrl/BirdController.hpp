#ifndef APP_CTRL_BIRDCONTROLLER_HPP_INCLUDED
#define APP_CTRL_BIRDCONTROLLER_HPP_INCLUDED

#include "../tl/Sync.hpp"
#include "../model/Bird.hpp"

#include <ppr/ctrl/Controller.hpp>

namespace app {
	namespace ctrl {
		class BirdController : public ppr::ctrl::Controller {
		private:
			tl::Sync<model::Bird>& m_bird;

		public:
			BirdController(tl::Sync<model::Bird>& bird);
			virtual void update(std::chrono::microseconds);
		};
	}
}

#endif//APP_CTRL_BIRDCONTROLLER_HPP_INCLUDED
