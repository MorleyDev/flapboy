#include "PipeController.hpp"
#include "../events/SpawnPipeEvent.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/EndGameEvent.hpp"
#include <cmath>

app::ctrl::PipeController::PipeController(app::tl::Sync<std::list<app::model::Pipe>>& pipes)
		: m_sineWave(0.0),
		  m_sineWaveMultiplier(0.0),
		  m_pipes(pipes),
		  m_randomEngine(),
		  m_pipeGapSizeGenerator(50, 90),
		  m_pipeHeightOffsetGenerator(0, 10) {

	onEvent<events::SpawnPipeEvent>([this](events::SpawnPipeEvent const& event) {
	    m_pipes.update([this, event](std::list<app::model::Pipe> thePipes) -> std::list<app::model::Pipe> {
	        auto pipe = genPipe();
	        pipe.x += event.xOffset - 50.0 * (1.0/60.0);
	        thePipes.push_back(pipe);
	        return thePipes;
	    });
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent) {
	    m_pipes.update([this](std::list<app::model::Pipe> thePipes) -> std::list<app::model::Pipe> {
		    thePipes.clear();
		    return thePipes;
	    });
	});

	onEvent<events::StartGameEvent>([this](events::StartGameEvent const& event) {

	    m_pipes.update([this, event](std::list<app::model::Pipe> thePipes) -> std::list<app::model::Pipe> {
	        thePipes.clear();
	        for (auto i = 0; i < event.settings.pipeCount; ++i)
		        emit<events::SpawnPipeEvent>({i * event.settings.pipeGapSizeX});
	        return thePipes;
	    });

	    std::lock_guard<std::mutex> lock(m_rngMutex);
	    m_pipeGapSizeGenerator = std::uniform_int_distribution<std::size_t>(event.settings.minGapSizeY, event.settings.maxGapSizeY);
	    m_sineWaveMultiplier.store(event.settings.pipeYWaveMultiplier);
	});
}

void app::ctrl::PipeController::update(std::chrono::microseconds dt) {
	m_pipes.update([this, dt](std::list<app::model::Pipe> pipes) -> std::list<app::model::Pipe> {
		for(auto i = pipes.begin(); i != pipes.end();) {
			if ( i->x < -10 ) {
				emit<events::SpawnPipeEvent>({i->x + 10.0});
				pipes.erase(i++);
			} else ++i;
		}
		return pipes;
	});
}

app::model::Pipe app::ctrl::PipeController::genPipe() {
	std::lock_guard<std::mutex> lock(m_rngMutex);
	double gapSize = static_cast<double>(m_pipeGapSizeGenerator(m_randomEngine));
	double halfGapSize = std::floor(gapSize / 2.0);
	double sineOffset = (std::floor(std::sin(m_sineWave) * m_sineWaveMultiplier) + static_cast<double>(m_pipeHeightOffsetGenerator(m_randomEngine)));

	double topHeight = 72.0 - halfGapSize + sineOffset;
	double bottomHeight = 72.0 - halfGapSize - sineOffset;

	m_sineWave += 0.2;

	return { topHeight, bottomHeight, 180.0, false };
}
