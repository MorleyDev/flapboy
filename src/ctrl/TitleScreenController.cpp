#include "TitleScreenController.hpp"
#include "../events/RequestJumpBirdEvent.hpp"
#include "../events/ChangeMenuOptionEvent.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/EndGameEvent.hpp"
#include "../events/RequestSelectEvent.hpp"
#include "../events/RequestMenuSwitchEvent.hpp"
#include <ppr/data/loaders/JsonLoader.hpp>
#include <ppr/events/Close.hpp>

app::ctrl::TitleScreenController::TitleScreenController(ppr::data::ContentLoader& content)
		: m_gameDifficultyMap(),
		  m_isActive(true),
		  m_activeMenuOption(0),
		  m_activeDifficulty(model::Difficulty::Easy) {

	m_gameDifficultyMap[model::Difficulty::Easy] = content.load<model::GameSettings>("data/easy.json");
	m_gameDifficultyMap[model::Difficulty::Medium] = content.load<model::GameSettings>("data/medium.json");
	m_gameDifficultyMap[model::Difficulty::Hard] = content.load<model::GameSettings>("data/hard.json");

	onEvent<events::RequestJumpBirdEvent>([this](events::RequestJumpBirdEvent) {
		if (!m_isActive.load())
			return;

		if (m_activeMenuOption.load() == 0)
			m_activeMenuOption.store(1);
		else
			m_activeMenuOption.store(0);

	    emit<events::ChangeMenuOptionEvent>({m_activeMenuOption.load(), m_activeDifficulty.load()});
	});
	onEvent<events::RequestMenuSwitchEvent>([this](events::RequestMenuSwitchEvent) {
		if (!m_isActive.load())
			return;
		if (m_activeMenuOption.load() != 0)
			return;

		auto currDiff = m_activeDifficulty.load();
		if (currDiff == model::Difficulty::Easy)
			m_activeDifficulty.store(model::Difficulty::Medium);
		else if (currDiff == model::Difficulty::Medium)
			m_activeDifficulty.store(model::Difficulty::Hard);
		else
			m_activeDifficulty.store(model::Difficulty::Easy);
	    emit<events::ChangeMenuOptionEvent>({m_activeMenuOption.load(), m_activeDifficulty.load()});
	});
	onEvent<events::StartGameEvent>([this](events::StartGameEvent) {
		m_isActive.store(false);
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent) {
	    m_isActive.store(true);
	});
	onEvent<events::RequestSelectEvent>([this](events::RequestSelectEvent) {
		if (!m_isActive.load())
			return;

		if (m_activeMenuOption.load() == 0)
			emit<events::StartGameEvent>({m_gameDifficultyMap[m_activeDifficulty.load()], m_activeDifficulty.load()});
		else
			emit<ppr::events::Close>({});
	});
}

void app::ctrl::TitleScreenController::update(std::chrono::microseconds microseconds) {

}
