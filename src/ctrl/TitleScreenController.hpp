#ifndef APP_CTRL_TITLESCREENCONTROLLER_HPP_INCLUDED
#define APP_CTRL_TITLESCREENCONTROLLER_HPP_INCLUDED

#include "../model/Difficulty.hpp"
#include "../model/GameSettings.hpp"

#include <ppr/ctrl/Controller.hpp>
#include <ppr/data/ContentLoader.hpp>
#include <map>

namespace app {
	namespace ctrl {
		class TitleScreenController : public ppr::ctrl::Controller {
		private:
			std::map<model::Difficulty, model::GameSettings> m_gameDifficultyMap;
			std::atomic<bool> m_isActive;
			std::atomic<std::size_t> m_activeMenuOption;
			std::atomic<model::Difficulty> m_activeDifficulty;

		public:
			TitleScreenController(ppr::data::ContentLoader& content);

			virtual void update(std::chrono::microseconds);
		};
	}
}

#endif//APP_CTRL_TITLESCREENCONTROLLER_HPP_INCLUDED
