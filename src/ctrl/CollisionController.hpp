#ifndef APP_CTRL_COLLISIONCONTROLLER_HPP_INCLUDED
#define APP_CTRL_COLLISIONCONTROLLER_HPP_INCLUDED

#include "../tl/Sync.hpp"
#include "../model/Bird.hpp"
#include "../model/Pipe.hpp"

#include <ppr/ctrl/Controller.hpp>
#include <list>

namespace app {
	namespace ctrl {
		class CollisionController : public ppr::ctrl::Controller {
		private:
			tl::Sync<model::Bird>& m_bird;
			tl::Sync<std::list<model::Pipe>>& m_pipes;
			std::atomic<bool> m_isGameOver;

		public:
			CollisionController(tl::Sync<model::Bird>& bird, tl::Sync<std::list<model::Pipe>>& pipes);

			virtual void update(std::chrono::microseconds dt);
		};
	}
}

#endif//APP_CTRL_COLLISIONCONTROLLER_HPP_INCLUDED
