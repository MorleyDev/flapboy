#ifndef APP_CTRL_BIRDPHYSICSCONTROLLER_HPP_INCLUDED
#define APP_CTRL_BIRDPHYSICSCONTROLLER_HPP_INCLUDED

#include "../tl/Sync.hpp"
#include "../model/Bird.hpp"
#include <ppr/ctrl/Controller.hpp>

namespace app {
	namespace ctrl {
		class BirdPhysicsController : public ppr::ctrl::Controller {
		private:
			tl::Sync<model::Bird>& m_bird;

		public:
			BirdPhysicsController(tl::Sync<model::Bird>&);
			virtual void update(std::chrono::microseconds);
		};
	}
}

#endif//APP_CTRL_BIRDPHYSICSCONTROLLER_HPP_INCLUDED
