#include "BirdController.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/EndGameEvent.hpp"
#include <ppr/tl/chrono.hpp>

app::ctrl::BirdController::BirdController(tl::Sync<model::Bird>& bird)
		: m_bird(bird) {
	onEvent<events::StartGameEvent>([this](events::StartGameEvent) {
		m_bird.update([](model::Bird brd) -> model::Bird {
			brd.position = ppr::model::pos2f(30.0, 20.0);
			brd.force = 0.0;
			brd.animation = 0.0;
			return brd;
		});
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent) {
	    m_bird.update([](model::Bird brd) -> model::Bird {
	        brd.position = ppr::model::pos2f(-30.0, 20.0);
	        brd.force = 0.0;
	        brd.animation = 0.0;
	        return brd;
	    });
	});
}

void app::ctrl::BirdController::update(std::chrono::microseconds microseconds) {
	m_bird.update([microseconds](model::Bird bird) -> model::Bird {
	    bird.animation -= ppr::tl::chrono::to_seconds(microseconds).count();
	    if (bird.animation < 0.0)
			bird.animation = 0.0;
		return bird;
	});
}
