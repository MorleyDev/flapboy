#include "PipeScrollingController.hpp"
#include "../events/GameOverEvent.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/EndGameEvent.hpp"
#include <ppr/tl/chrono.hpp>

app::ctrl::PipeScrollingController::PipeScrollingController(app::tl::Sync<std::list<app::model::Pipe>>& pipes)
		: m_pipes(pipes),
		  m_pipeSpeed(0.0) {

	onEvent<events::StartGameEvent>([this](events::StartGameEvent event) {
	    m_pipeSpeed.store(event.settings.pipeScrollSpeed);
	});
	onEvent<events::GameOverEvent>([this](events::GameOverEvent) {
	    m_pipeSpeed.store(0.0);
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent) {
	    m_pipeSpeed.store(0.0);
	});
}

void app::ctrl::PipeScrollingController::update(std::chrono::microseconds dt) {

	m_pipes.update([this, dt](std::list<app::model::Pipe> pipes) -> std::list<app::model::Pipe> {
	    for(auto& pipe : pipes) {
		    pipe.x -= (m_pipeSpeed.load() * ppr::tl::chrono::to_seconds(dt).count());
	    }

	    return pipes;
	});
}
