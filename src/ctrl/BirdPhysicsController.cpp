#include "BirdPhysicsController.hpp"
#include <ppr/tl/chrono.hpp>

app::ctrl::BirdPhysicsController::BirdPhysicsController(app::tl::Sync<app::model::Bird>& bird)
		: m_bird(bird) {

}

void app::ctrl::BirdPhysicsController::update(std::chrono::microseconds dt) {
	m_bird.update([this, dt](model::Bird bird) -> model::Bird {
	    bird.position.y += (bird.force * ppr::tl::chrono::to_seconds(dt).count());
		bird.force += (1000.0 * ppr::tl::chrono::to_seconds(dt).count());
		return bird;
	});
}
