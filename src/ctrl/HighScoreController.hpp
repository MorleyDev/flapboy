#ifndef APP_CTRL_HIGHSCORECONTROLLER_HPP_INCLUDED
#define APP_CTRL_HIGHSCORECONTROLLER_HPP_INCLUDED

#include "../tl/Sync.hpp"
#include "../model/Highscore.hpp"
#include "../model/Difficulty.hpp"
#include <ppr/ctrl/Controller.hpp>
#include <mutex>

namespace app {
	namespace ctrl {
		class HighScoreController : public ppr::ctrl::Controller {
		private:
			std::mutex m_fileLock;
			tl::Sync<model::Highscore>& m_highscore;
			std::atomic<std::size_t>& m_score;
			std::atomic<model::Difficulty> m_difficulty;

		public:
			HighScoreController(tl::Sync<model::Highscore>& highscore, std::atomic<std::size_t>& score);

			virtual void update(std::chrono::microseconds dt);

			void updateHighScore();
		};
	}
}

#endif//APP_CTRL_HIGHSCORECONTROLLER_HPP_INCLUDED
