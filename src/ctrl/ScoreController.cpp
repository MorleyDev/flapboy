#include "ScoreController.hpp"
#include "../events/AwardPointEvent.hpp"
#include "../events/StartGameEvent.hpp"

void app::ctrl::ScoreController::update(std::chrono::microseconds microseconds) {

	auto bird = m_bird.get();
	m_pipes.update([this, bird](std::list<model::Pipe> pipes) -> std::list<model::Pipe> {
	    for(auto& pipe : pipes) {
		    if (!pipe.wasPassed && pipe.x < (bird.position.x - 10.0)) {
			    emit<events::AwardPointEvent>({});
			    pipe.wasPassed = true;
		    }
	    }
		return pipes;
	});
}

app::ctrl::ScoreController::ScoreController(std::atomic<std::size_t>& score, app::tl::Sync<app::model::Bird>& bird, app::tl::Sync<std::list<app::model::Pipe>>& pipes)
		: m_score(score),
		  m_bird(bird),
		  m_pipes(pipes) {
	onEvent<events::AwardPointEvent>([this](events::AwardPointEvent) {
		++m_score;
	});
	onEvent<events::StartGameEvent>([this](events::StartGameEvent) {
	    m_score.store(0);
	});
}
