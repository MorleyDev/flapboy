#ifndef APP_CTRL_GAMECONTROLLER_HPP_INCLUDED
#define APP_CTRL_GAMECONTROLLER_HPP_INCLUDED

#include "../model/GameSettings.hpp"
#include "../model/Difficulty.hpp"
#include <ppr/ctrl/Controller.hpp>
#include <mutex>

namespace app {
    namespace ctrl {
        class GameController : public ppr::ctrl::Controller {
        private:
	        std::atomic<bool> m_gameOverTimerStarted;

	        std::mutex m_mutex;
	        std::atomic<double> m_timerToGameRestart;
	        model::GameSettings m_settingsOfCurrentGame;
	        model::Difficulty m_difficultyOfCurrentGame;

        public:
	        GameController();

	        virtual void update(std::chrono::microseconds);
        };
    }
}

#endif//APP_CTRL_GAMECONTROLLER_HPP_INCLUDED
