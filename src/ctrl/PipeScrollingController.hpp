#ifndef APP_CTRL_PIPESCROLLINGCONTROLLER_HPP_INCLUDED
#define APP_CTRL_PIPESCROLLINGCONTROLLER_HPP_INCLUDED

#include <ppr/ctrl/Controller.hpp>
#include "../tl/Sync.hpp"
#include "../model/Pipe.hpp"
#include <list>
#include <random>

namespace app {
    namespace ctrl {
        class PipeScrollingController : public ppr::ctrl::Controller {
        private:
	        tl::Sync<std::list<model::Pipe>>& m_pipes;
	        std::atomic<double> m_pipeSpeed;

        public:
	        PipeScrollingController(tl::Sync<std::list<model::Pipe>>& pipes);

	        virtual void update(std::chrono::microseconds dt);
        };
    }
}

#endif//APP_CTRL_PIPESCROLLINGCONTROLLER_HPP_INCLUDED
