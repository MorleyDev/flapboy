#ifndef APP_CTRL_BIRDJUMPCONTROLLER_HPP_INCLUDED
#define APP_CTRL_BIRDJUMPCONTROLLER_HPP_INCLUDED

#include <ppr/ctrl/Controller.hpp>
#include "../tl/Sync.hpp"
#include "../model/Bird.hpp"

namespace app {
	namespace ctrl {
		class BirdJumpController : public ppr::ctrl::Controller {
		private:
			tl::Sync<model::Bird>& m_bird;
			std::atomic<bool> m_canJump;

		public:
			BirdJumpController(tl::Sync<model::Bird>& bird);

			virtual void update(std::chrono::microseconds) { }
		};
	}
}

#endif//APP_CTRL_BIRDJUMPCONTROLLER_HPP_INCLUDED

