#include "BirdJumpController.hpp"
#include "../events/RequestJumpBirdEvent.hpp"
#include "../events/BirdJumpedEvent.hpp"
#include "../events/GameOverEvent.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/EndGameEvent.hpp"

app::ctrl::BirdJumpController::BirdJumpController(app::tl::Sync<app::model::Bird>& bird)
	: m_bird(bird),
	  m_canJump(false) {
	onEvent<app::events::RequestJumpBirdEvent>([this](app::events::RequestJumpBirdEvent) {
	    m_bird.update([this](model::Bird brd) -> model::Bird {
	        if (m_canJump.load()) {
		        brd.force = -200.0;
		        if (brd.animation > 0.0)
			        brd.animation = 0.0;
		        else
			        brd.animation = 0.2;
		        emit<events::BirdJumpedEvent>({});
	        }
	        return brd;
	    });
	});

	onEvent<app::events::StartGameEvent>([this](app::events::StartGameEvent) {
	    m_canJump.store(true);
	});

	onEvent<app::events::GameOverEvent>([this](app::events::GameOverEvent) {
	    m_canJump.store(false);
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent) {
	    m_canJump.store(false);
	});
}
