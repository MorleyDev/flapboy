#include "CollisionController.hpp"
#include "../events/GameOverEvent.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/EndGameEvent.hpp"
#include <ppr/model/rectangle.hpp>

app::ctrl::CollisionController::CollisionController(app::tl::Sync<app::model::Bird>& bird, app::tl::Sync<std::list<app::model::Pipe>>& pipes)
		: m_bird(bird),
		  m_pipes(pipes),
		  m_isGameOver(true) {
	onEvent<events::StartGameEvent>([this](events::StartGameEvent) {
	    m_isGameOver.store(false);
	});
	onEvent<events::GameOverEvent>([this](events::GameOverEvent) {
	    m_isGameOver.store(true);
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent) {
	    m_isGameOver.store(true);
	});
}

namespace {
	template<typename T>
	bool AreColliding(ppr::model::rectangle<T> a, ppr::model::rectangle<T> b) {
		return (a.x <= b.x + b.width &&
				a.x + a.width >= b.x &&
				a.y <= b.y + b.height &&
				a.height + a.y >= b.y);
	}
}

void app::ctrl::CollisionController::update(std::chrono::microseconds dt) {
	if (m_isGameOver.load())
		return;

	auto bird = m_bird.get();
	auto pipes = m_pipes.get();

	if (bird.position.y < -10.0 || bird.position.y > 154) {
		emit(events::GameOverEvent());
		return;
	}

	const ppr::model::rectf birdArea(bird.position.x - 9.0, bird.position.y - 5.0, 18.0, 10.0);
	for(auto const& pipe : pipes) {
		const ppr::model::rectf topPipeArea(pipe.x - 10.0, 0.0, 20.0, pipe.topHeight + 7.0);
		const ppr::model::rectf bottomPipeArea(pipe.x - 10.0, 144 - pipe.bottomHeight - 7.0, 20.0, pipe.bottomHeight + 7.0);

		if (AreColliding(birdArea, topPipeArea) || AreColliding(birdArea, bottomPipeArea)) {
			emit(events::GameOverEvent());
			return;
		}
	}
}
