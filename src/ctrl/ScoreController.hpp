#ifndef APP_CTRL_SCORECONTROLLER_HPP_INCLUDED
#define APP_CTRL_SCORECONTROLLER_HPP_INCLUDED

#include "../model/Bird.hpp"
#include "../model/Pipe.hpp"
#include "../tl/Sync.hpp"

#include <ppr/ctrl/Controller.hpp>
#include <atomic>
#include <list>

namespace app {
	namespace ctrl {
		class ScoreController : public ppr::ctrl::Controller {
		private:
			std::atomic<std::size_t>& m_score;
			tl::Sync<model::Bird>& m_bird;
			tl::Sync<std::list<model::Pipe>>& m_pipes;

		public:
			ScoreController(std::atomic<std::size_t>& score, tl::Sync<model::Bird>&, tl::Sync<std::list<model::Pipe>>& pipes);

			virtual void update(std::chrono::microseconds);
		};
	}
}

#endif//APP_CTRL_SCORECONTROLLER_HPP_INCLUDED
