#include "GameController.hpp"
#include "../events/StartGameEvent.hpp"
#include "../events/GameOverEvent.hpp"
#include "../events/EndGameEvent.hpp"
#include <ppr/tl/chrono.hpp>

app::ctrl::GameController::GameController()
		: m_gameOverTimerStarted(false),
		  m_mutex(),
		  m_timerToGameRestart(0.0),
		  m_settingsOfCurrentGame() {

	onEvent<events::GameOverEvent>([this](events::GameOverEvent) {
	    std::lock_guard<std::mutex> lock(m_mutex);
	    m_timerToGameRestart.store(0.0);
		m_gameOverTimerStarted.store(true);
	});
	onEvent<events::EndGameEvent>([this](events::EndGameEvent) {
	    std::lock_guard<std::mutex> lock(m_mutex);
	    m_timerToGameRestart.store(0.0);
	    m_gameOverTimerStarted.store(false);
	});
	onEvent<events::StartGameEvent>([this](events::StartGameEvent e) {
	    std::lock_guard<std::mutex> lock(m_mutex);
	    m_gameOverTimerStarted.store(false);
	    m_timerToGameRestart.store(0.0);
	    m_settingsOfCurrentGame = e.settings;
	    m_difficultyOfCurrentGame = e.difficulty;
	});

}

void app::ctrl::GameController::update(std::chrono::microseconds dt) {
	if (!m_gameOverTimerStarted.load())
		return;

	std::lock_guard<std::mutex> lock(m_mutex);
	auto time = m_timerToGameRestart.load() + ppr::tl::chrono::to_seconds(dt).count();
	m_timerToGameRestart.store(time);

	if (time >= 1.0) {
		m_gameOverTimerStarted.store(false);

		emit<events::StartGameEvent>({m_settingsOfCurrentGame, m_difficultyOfCurrentGame});
		return;
	}
}
