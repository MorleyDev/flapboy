#include "MainGame.hpp"
#include <ppr/run.hpp>

#include <iostream>

int main(int nArgs, const char** ppcArgs) {

	try {
		app::MainGame game;
		ppr::run(game);
	} catch(std::exception const& e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}
	return 0;
}
