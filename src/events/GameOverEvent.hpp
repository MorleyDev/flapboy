#ifndef APP_EVENTS_GAMEOVEREVENT_HPP_INCLUDED
#define APP_EVENTS_GAMEOVEREVENT_HPP_INCLUDED

namespace app {
    namespace events {
        struct GameOverEvent { };
    }
}

#endif//APP_EVENTS_GAMEOVEREVENT_HPP_INCLUDED
