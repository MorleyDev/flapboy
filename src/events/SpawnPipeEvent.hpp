#ifndef APP_EVENTS_SPAWNPIPEEVENT_HPP_INCLUDED
#define APP_EVENTS_SPAWNPIPEEVENT_HPP_INCLUDED

namespace app {
    namespace events {
        struct SpawnPipeEvent {
            double xOffset;
        };
    }
}

#endif//APP_EVENTS_SPAWNPIPEEVENT_HPP_INCLUDED
