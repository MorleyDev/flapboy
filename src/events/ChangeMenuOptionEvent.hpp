#ifndef APP_EVENTS_CHANGEMENUOPTIONEVENT_HPP_INCLUDED
#define APP_EVENTS_CHANGEMENUOPTIONEVENT_HPP_INCLUDED

#include "../model/Difficulty.hpp"
#include <cstdint>

namespace app {
	namespace events {
		struct ChangeMenuOptionEvent {
			std::size_t option;
			model::Difficulty difficulty;
		};
	}
}

#endif//APP_EVENTS_CHANGEMENUOPTIONEVENT_HPP_INCLUDED

