#ifndef APP_EVENTS_STARTGAMEEVENT_HPP_INCLUDED
#define APP_EVENTS_STARTGAMEEVENT_HPP_INCLUDED

#include "../model/GameSettings.hpp"
#include "../model/Difficulty.hpp"

namespace app {
    namespace events {
        struct StartGameEvent {
	        model::GameSettings settings;
	        model::Difficulty difficulty;
        };
    }
}

#endif//APP_EVENTS_STARTGAMEEVENT_HPP_INCLUDED
