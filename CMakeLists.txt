cmake_minimum_required (VERSION 2.6) 
project(PAUPER CXX) 

include(cxx11.cmake)
include(g.cmake)
include(sanitizer.cmake)
include(zander.cmake)
include(util.cmake)

add_subdirectory(resources)
add_subdirectory(scripts)
add_subdirectory(src)

# enable_testing()
# add_subdirectory(test)
