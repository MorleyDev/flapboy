#########################################################
# Utilise the zander system to acquire the dependencies #
#########################################################

option(ENABLE_ZANDER "Enable or disable usage of the zander cxx dependency management system" ON)
option(RUN_ZANDER_MODE "Used to trigger zander in execution mode" OFF)

if (RUN_ZANDER_MODE)
	file(MAKE_DIRECTORY ${ZANDER_EXT_DIRECTORY})
	file(MAKE_DIRECTORY ${ZANDER_EXT_DIRECTORY}/include)
	file(MAKE_DIRECTORY ${ZANDER_EXT_DIRECTORY}/bin)
	file(MAKE_DIRECTORY ${ZANDER_EXT_DIRECTORY}/lib)

	separate_arguments(ZANDER_DEPENDENCIES)
	foreach(ZANDER_DEPENDENCY ${ZANDER_DEPENDENCIES})
		message("zander-client get ${ZANDER_DEPENDENCY} ${ZANDER_COMPILER} ${ZANDER_BUILD_TYPE}")
		if (WIN32)
			execute_process(COMMAND cmd /c zander-client get ${ZANDER_DEPENDENCY} ${ZANDER_COMPILER} ${ZANDER_BUILD_TYPE} WORKING_DIRECTORY ${ZANDER_EXT_DIRECTORY})
		else()
			execute_process(COMMAND bash -c "zander-client get ${ZANDER_DEPENDENCY} ${ZANDER_COMPILER} ${ZANDER_BUILD_TYPE}" WORKING_DIRECTORY ${ZANDER_EXT_DIRECTORY})
		endif()
	endforeach(ZANDER_DEPENDENCY)

else() # NOT RUN_ZANDER_MODE
	if(ENABLE_ZANDER)
		set(ZANDER_EXT_DIRECTORY "${CMAKE_BINARY_DIR}/ext" CACHE FILEPATH "Path to external includes")

		if(CMAKE_BUILD_TYPE STREQUAL "Debug")
			set(ZANDER_BUILD_TYPE "debug")
		else()
			set(ZANDER_BUILD_TYPE "release")
		endif()

		if(CMAKE_GENERATOR STREQUAL "MinGW Makefiles")
			set(ZANDER_COMPILER "mingw" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "Unix Makefiles")
			set(ZANDER_COMPILER "unix" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "MSYS Makefiles")
			set(ZANDER_COMPILER "msys" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "Borland Makefiles")
			set(ZANDER_COMPILER "borland" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "NMake Makefiles")
			set(ZANDER_COMPILER "nmake" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "NMake Makefiles JOM")
			set(ZANDER_COMPILER "nmake-jom" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "Visual Studio 10")
			set(ZANDER_COMPILER "msvc10" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "Visual Studio 11")
			set(ZANDER_COMPILER "msvc11" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "Visual Studio 12")
			set(ZANDER_COMPILER "msvc12" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "Visual Studio 10 Win64")
			set(ZANDER_COMPILER "msvc10w64" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "Visual Studio 11 Win64")
			set(ZANDER_COMPILER "msvc11w64" CACHE STRING "Zander Compiler Flag")
		elseif(CMAKE_GENERATOR STREQUAL "Visual Studio 12 Win64")
			set(ZANDER_COMPILER "msvc12w64" CACHE STRING "Zander Compiler Flag")
		elseif(NOT ZANDER_COMPILER)
			set(ZANDER_UNKNOWN_COMPILER ON)
		endif()

		include_directories(${ZANDER_EXT_DIRECTORY}/include)
		link_directories(${ZANDER_EXT_DIRECTORY}/lib)

		install(DIRECTORY ${ZANDER_EXT_DIRECTORY}/bin/ ${ZANDER_EXT_DIRECTORY}/lib/ DESTINATION lib FILES_MATCHING PATTERN "*.dll")
		install(DIRECTORY ${ZANDER_EXT_DIRECTORY}/bin/ ${ZANDER_EXT_DIRECTORY}/lib/ DESTINATION lib FILES_MATCHING PATTERN "*.so*")
	endif() # ENABLE_ZANDER

	function(add_zander_dependencies ZNDTARGET)
		if(NOT ZANDER_UNKNOWN_COMPILER AND ENABLE_ZANDER)
			foreach(ZND_DEP_ARG ${ARGN})
				if(NOT TARGET ${ZND_DEP_ARG}_zander)
					add_custom_target(${ZND_DEP_ARG}_zander DEPENDS ${ZND_DEP_ARG}_zander.tmp COMMAND ${CMAKE_COMMAND} -E touch ${ZND_DEP_ARG}_zander.tmp)
					add_custom_command(OUTPUT ${ZND_DEP_ARG}_zander.tmp
									   COMMAND ${CMAKE_COMMAND} -DRUN_ZANDER_MODE=ON
																-DZANDER_EXT_DIRECTORY=${ZANDER_EXT_DIRECTORY}
																-DZANDER_DEPENDENCIES="${ZND_DEP_ARG}"
																-DZANDER_COMPILER=${ZANDER_COMPILER}
																-DZANDER_BUILD_TYPE=${ZANDER_BUILD_TYPE}
																-P ${CMAKE_SOURCE_DIR}/zander.cmake)
				endif()
				add_dependencies(${ZNDTARGET} ${ZND_DEP_ARG}_zander)
			endforeach()
		endif() # NOT ZANDER_UNKNOWN_COMPILER AND ENABLE_ZANDER
	endfunction(add_zander_dependencies)
endif()
